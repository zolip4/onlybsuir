<?php

use yii\db\Migration;

/**
 * Handles the creation of table `abit`.
 */
class m180617_233541_create_abit_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('abit', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('abit');
    }
}
