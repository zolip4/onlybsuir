<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Очередь';
?>
<div class="abit-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Зарегистрироваться', ['create'], ['class' => 'btn btn-success']) ?>
        <?= Html::a('babka room', ['babka'], ['class' => 'btn btn-success']) ?>

    </p>
    <?php Pjax::begin(); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
