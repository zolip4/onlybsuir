<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Abit */

$this->title = 'Create Abit';
$this->params['breadcrumbs'][] = ['label' => 'Abits', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="abit-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
